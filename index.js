const library2 = require('library2')

module.exports = {
  hello: function (message) {
  	console.log('hello ' + message + ', ' + library2.welcome())
  },
  yo: function () {
  	console.log('Yo!')
  }
}
